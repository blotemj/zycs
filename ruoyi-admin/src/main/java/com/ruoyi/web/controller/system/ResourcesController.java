package com.ruoyi.web.controller.system;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.charset.StandardCharsets;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.google.gson.Gson;
import com.ruoyi.framework.web.domain.server.Sys;
import com.ruoyi.system.domain.Resources;
import com.ruoyi.system.domain.Response;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.LxmResources;
import com.ruoyi.system.service.ILxmResourcesService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

import javax.servlet.http.HttpServletResponse;

/**
 * 资源Controller
 *
 * @author ruoyi
 * @date 2024-10-22
 */
@Controller
@RequestMapping("/app/article")
public class ResourcesController extends BaseController {
    private String prefix = "app/article";

    @Autowired
    private ILxmResourcesService lxmResourcesService;

    @RequiresPermissions("system:resources:view")
    @GetMapping()
    public String resources() {
        return prefix + "/article";
    }

    /**
     * 查询资源列表
     */
    @RequiresPermissions("system:resources:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(LxmResources lxmResources) {
        startPage();
        List<LxmResources> list = lxmResourcesService.selectLxmResourcesList(lxmResources);
        return getDataTable(list);
    }

    /**
     * 导出资源列表
     */
    @RequiresPermissions("system:resources:export")
    @Log(title = "资源", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(LxmResources lxmResources) {
        List<LxmResources> list = lxmResourcesService.selectLxmResourcesList(lxmResources);
        ExcelUtil<LxmResources> util = new ExcelUtil<LxmResources>(LxmResources.class);
        return util.exportExcel(list, "资源数据");
    }

    /**
     * 新增资源
     */
    @GetMapping("/add")
    public String add() {
        return prefix + "/add";
    }

    /**
     * 新增保存资源
     */
    @RequiresPermissions("system:resources:add")
    @Log(title = "资源", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(LxmResources lxmResources) {
        return toAjax(lxmResourcesService.insertLxmResources(lxmResources));
    }

    /**
     * 修改资源
     */
    @RequiresPermissions("system:resources:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap) {
        LxmResources lxmResources = lxmResourcesService.selectLxmResourcesById(id);
        mmap.put("lxmResources", lxmResources);
        return prefix + "/edit";
    }

    /**
     * 修改保存资源
     */
    @RequiresPermissions("system:resources:edit")
    @Log(title = "资源", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(LxmResources lxmResources) {
        return toAjax(lxmResourcesService.updateLxmResources(lxmResources));
    }

    /**
     * 删除资源
     */
    @RequiresPermissions("system:resources:remove")
    @Log(title = "资源", businessType = BusinessType.DELETE)
    @PostMapping("/remove")
    @ResponseBody
    public AjaxResult remove(String ids) {
        return toAjax(lxmResourcesService.deleteLxmResourcesByIds(ids));
    }

    //    ---------------------------新增-------------------------------

    //响应请求返回json
    private void out(String json) {
        HttpServletResponse response = getResponse();
        response.setContentType("text/html");
        response.setCharacterEncoding("utf-8");
        response.setHeader("Content-type", "text/html;charset=UTF-8");
        try {
            OutputStream outputStream = response.getOutputStream();
            outputStream.write(json.toString().getBytes(StandardCharsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * search resources
     *
     * @param lxmResources
     */
    @Log(title = "搜索资源", businessType = BusinessType.DELETE)
    @GetMapping("/search")
    @ResponseBody
    public void search(LxmResources lxmResources) {

        List<LxmResources> list = lxmResourcesService.selectLxmResourcesList(lxmResources);
        if (list.size() < 20) {
            Gson gson = new Gson();
            String json = gson.toJson(list);
            out(json);
        }
    }

    //    http://127.0.0.1:8080/app/article/list?pageNum=1&pageSize=7&isShow=1&keyword=你好&orderType=look_number
    @Log(title = "搜索资源1", businessType = BusinessType.DELETE)
    @GetMapping("/list")
    @ResponseBody
    public Response search1(@RequestParam(value = "pageNum", required = false) int pageNum,
                            @RequestParam(value = "pageSize", required = false) int pageSize,
                            @RequestParam(value = "isShow", required = false) int isShow,
                            @RequestParam(value = "keyword", required = false) String keyword,
                            @RequestParam(value = "orderType", required = false) String orderType) {
        // 处理业务逻辑
        System.out.println("Page Number: " + pageNum);
        System.out.println("Page Size: " + pageSize);
        System.out.println("Is Show: " + isShow);
        System.out.println("Keyword: " + keyword);
        System.out.println("Order Type: " + orderType);

        //返回
        Resources blogItem1 = new Resources();
        blogItem1.setId(1);
        blogItem1.setAd(0);
        blogItem1.setShowImg("images/blog1.jpg");
        blogItem1.setTitle("这是一个博客标题");
        blogItem1.setIsAd(0);
        blogItem1.setAppResourceId(null);

        Date currentDate = new Date();
        blogItem1.setCreateTime(currentDate);
        blogItem1.setLookNumber(123);

        Resources blogItem2 = new Resources();
        blogItem2.setId(2);
        blogItem2.setAd(1);
        blogItem2.setShowImg("");
        blogItem2.setTitle("");
        blogItem2.setIsAd(0);
        blogItem2.setAppResourceId(null);
        blogItem2.setCreateTime(currentDate);
        blogItem2.setLookNumber(0);


        List<Resources> blogList = Arrays.asList(blogItem1, blogItem2);

        Response response = new Response();
        response.setBlogList(blogList);
        response.setTotal(2);
        response.setAdUnitId("111");
        response.setBaseUrl("http://127.0.0.1:8080/");


//        System.out.println("Response: " + response);

        return response;
    }
}
