package com.ruoyi.system.service;

import java.util.List;
import com.ruoyi.system.domain.LxmResources;

/**
 * 资源Service接口
 * 
 * @author ruoyi
 * @date 2024-10-22
 */
public interface ILxmResourcesService 
{
    /**
     * 查询资源
     * 
     * @param id 资源主键
     * @return 资源
     */
    public LxmResources selectLxmResourcesById(Long id);

    /**
     * 查询资源列表
     * 
     * @param lxmResources 资源
     * @return 资源集合
     */
    public List<LxmResources> selectLxmResourcesList(LxmResources lxmResources);

    /**
     * 新增资源
     * 
     * @param lxmResources 资源
     * @return 结果
     */
    public int insertLxmResources(LxmResources lxmResources);

    /**
     * 修改资源
     * 
     * @param lxmResources 资源
     * @return 结果
     */
    public int updateLxmResources(LxmResources lxmResources);

    /**
     * 批量删除资源
     * 
     * @param ids 需要删除的资源主键集合
     * @return 结果
     */
    public int deleteLxmResourcesByIds(String ids);

    /**
     * 删除资源信息
     * 
     * @param id 资源主键
     * @return 结果
     */
    public int deleteLxmResourcesById(Long id);
}
