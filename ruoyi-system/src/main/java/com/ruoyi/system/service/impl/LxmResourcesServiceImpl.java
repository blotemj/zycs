package com.ruoyi.system.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.LxmResourcesMapper;
import com.ruoyi.system.domain.LxmResources;
import com.ruoyi.system.service.ILxmResourcesService;
import com.ruoyi.common.core.text.Convert;

/**
 * 资源Service业务层处理
 * 
 * @author ruoyi
 * @date 2024-10-22
 */
@Service
public class LxmResourcesServiceImpl implements ILxmResourcesService 
{
    @Autowired
    private LxmResourcesMapper lxmResourcesMapper;

    /**
     * 查询资源
     * 
     * @param id 资源主键
     * @return 资源
     */
    @Override
    public LxmResources selectLxmResourcesById(Long id)
    {
        return lxmResourcesMapper.selectLxmResourcesById(id);
    }

    /**
     * 查询资源列表
     * 
     * @param lxmResources 资源
     * @return 资源
     */
    @Override
    public List<LxmResources> selectLxmResourcesList(LxmResources lxmResources)
    {
        return lxmResourcesMapper.selectLxmResourcesList(lxmResources);
    }

    /**
     * 新增资源
     * 
     * @param lxmResources 资源
     * @return 结果
     */
    @Override
    public int insertLxmResources(LxmResources lxmResources)
    {
        return lxmResourcesMapper.insertLxmResources(lxmResources);
    }

    /**
     * 修改资源
     * 
     * @param lxmResources 资源
     * @return 结果
     */
    @Override
    public int updateLxmResources(LxmResources lxmResources)
    {
        return lxmResourcesMapper.updateLxmResources(lxmResources);
    }

    /**
     * 批量删除资源
     * 
     * @param ids 需要删除的资源主键
     * @return 结果
     */
    @Override
    public int deleteLxmResourcesByIds(String ids)
    {
        return lxmResourcesMapper.deleteLxmResourcesByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除资源信息
     * 
     * @param id 资源主键
     * @return 结果
     */
    @Override
    public int deleteLxmResourcesById(Long id)
    {
        return lxmResourcesMapper.deleteLxmResourcesById(id);
    }
}
