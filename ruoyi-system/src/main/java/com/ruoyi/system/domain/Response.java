package com.ruoyi.system.domain;

import java.util.List;

public class Response {
    private List<Resources> blogList;
    private String adUnitId;
    private String baseUrl;
    private int total;

    public List<Resources> getBlogList() {
        return blogList;
    }

    public void setBlogList(List<Resources> blogList) {
        this.blogList = blogList;
    }

    public String getAdUnitId() {
        return adUnitId;
    }

    public void setAdUnitId(String adUnitId) {
        this.adUnitId = adUnitId;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

    public void setBaseUrl(String baseUrl) {
        this.baseUrl = baseUrl;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }
}
