package com.ruoyi.system.domain;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 资源对象 lxm_resources
 * 
 * @author ruoyi
 * @date 2024-10-22
 */
public class LxmResources extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** ID */
    private Long id;

    /** 资源名称 */
    @Excel(name = "资源名称")
    private String name;

    /** 资源URL链接 */
    @Excel(name = "资源URL链接")
    private String url;

    /** 验证码 */
    @Excel(name = "验证码")
    private String verificationCode;

    /** 资源类别 */
    @Excel(name = "资源类别")
    private String resourceCategories;

    /** 网盘名称 */
    @Excel(name = "网盘名称")
    private String networkdiskName;

    /** 提交时间 */
    @JsonFormat(pattern = "yyyy-MM-dd")
    @Excel(name = "提交时间", width = 30, dateFormat = "yyyy-MM-dd")
    private Date commitTime;

    /** 备注 */
    @Excel(name = "备注")
    private String remarks;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }

    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }

    public void setUrl(String url) 
    {
        this.url = url;
    }

    public String getUrl() 
    {
        return url;
    }

    public void setVerificationCode(String verificationCode) 
    {
        this.verificationCode = verificationCode;
    }

    public String getVerificationCode() 
    {
        return verificationCode;
    }

    public void setResourceCategories(String resourceCategories) 
    {
        this.resourceCategories = resourceCategories;
    }

    public String getResourceCategories() 
    {
        return resourceCategories;
    }

    public void setNetworkdiskName(String networkdiskName) 
    {
        this.networkdiskName = networkdiskName;
    }

    public String getNetworkdiskName() 
    {
        return networkdiskName;
    }

    public void setCommitTime(Date commitTime) 
    {
        this.commitTime = commitTime;
    }

    public Date getCommitTime() 
    {
        return commitTime;
    }

    public void setRemarks(String remarks) 
    {
        this.remarks = remarks;
    }

    public String getRemarks() 
    {
        return remarks;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("url", getUrl())
            .append("verificationCode", getVerificationCode())
            .append("resourceCategories", getResourceCategories())
            .append("networkdiskName", getNetworkdiskName())
            .append("commitTime", getCommitTime())
            .append("remarks", getRemarks())
            .toString();
    }
}
