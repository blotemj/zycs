package com.ruoyi.system.domain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;

import java.util.Date;

/**
 * 资源对象 resources
 * 
 * @author ruoyi
 * @date 2024-10-22
 */
public class Resources extends BaseEntity
{
    private static final long serialVersionUID = 1L;



    private int id;
    private int ad;
    private String showImg;
    private String title;
    private int isAd;
    private Integer appResourceId;
    private Date createTime;
    private int lookNumber;

    // Getters and Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAd() {
        return ad;
    }

    public void setAd(int ad) {
        this.ad = ad;
    }

    public String getShowImg() {
        return showImg;
    }

    public void setShowImg(String showImg) {
        this.showImg = showImg;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getIsAd() {
        return isAd;
    }

    public void setIsAd(int isAd) {
        this.isAd = isAd;
    }

    public Integer getAppResourceId() {
        return appResourceId;
    }

    public void setAppResourceId(Integer appResourceId) {
        this.appResourceId = appResourceId;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public int getLookNumber() {
        return lookNumber;
    }

    public void setLookNumber(int lookNumber) {
        this.lookNumber = lookNumber;
    }

    @Override
    public String toString() {
        return "BlogItem{" +
                "id=" + id +
                ", ad=" + ad +
                ", showImg='" + showImg + '\'' +
                ", title='" + title + '\'' +
                ", isAd=" + isAd +
                ", appResourceId=" + appResourceId +
                ", createTime='" + createTime + '\'' +
                ", lookNumber=" + lookNumber +
                '}';
    }
}
